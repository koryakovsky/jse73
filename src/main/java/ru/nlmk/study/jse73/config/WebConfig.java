package ru.nlmk.study.jse73.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@ComponentScan({"ru.nlmk.study.jse73.controller", "ru.nlmk.study.jse73.config"})
public class WebConfig implements WebMvcConfigurer {
}
