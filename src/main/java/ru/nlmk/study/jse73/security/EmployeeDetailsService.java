package ru.nlmk.study.jse73.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nlmk.study.jse73.model.Employee;
import ru.nlmk.study.jse73.repository.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeDetailsService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee emp = employeeRepository.findByUsername(username);

        if (emp == null) {
            throw new UsernameNotFoundException(username + " not found.");
        }

        List<SimpleGrantedAuthority> roleList = new ArrayList<>(1);
        roleList.add(new SimpleGrantedAuthority("ROLE_USER"));

        return new User(emp.getUsername(), emp.getPassword(), roleList);
    }
}
