package ru.nlmk.study.jse73.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;

@Data
@Configuration
public class JwtTokenProvider {

    @Value("${jwt.token.header}")
    private String jwtHeader;

    @Value("${jwt.token.prefix}")
    private String jwtPrefix;

    @Value("${jwt.token.secret-key}")
    private String jwtSecret;

    @Value("${jwt.token.exptime.min}")
    private long jwtExpTime;

    public String generateToken(String username) {
        Date date = new Date(
                System.currentTimeMillis() + (jwtExpTime * 60 * 1000));

        Key key = Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8));
        Claims claims = Jwts.claims().setSubject(username);

        return jwtPrefix
                .concat(Jwts.builder()
                        .setClaims(claims)
                        .signWith(key, SignatureAlgorithm.HS512)
                        .setExpiration(date)
                        .compact()
                );
    }

    public String validateAndGetUserFromToken(String token){
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8)))
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            return claims.getSubject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
